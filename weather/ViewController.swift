	//
//  ViewController.swift
//  weather
//
//  Created by johnny on 4/18/18.
//  Copyright © 2018 TengoFree Studio. All rights reserved.
//

import UIKit
import CoreLocation
    
class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var tfCityName: UITextField!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    var weather: String!
    var cityName: String!
    var temp: Int!
    var locationManager:CLLocationManager!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "clima.jpg")!)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        determineMyCurrentLocation()
    }
    
    @IBAction func getWeather(_ sender: UIButton) {
        
        print("Datos capturados: \(tfCityName.text)")
        getWeatherFromApi(query: "q=\(self.tfCityName.text!)")
    }
    
    /*
     * get weather json from api 
     * params? query
     */
    func getWeatherFromApi(query: String ){
        let urlPath = "http://api.openweathermap.org/data/2.5/weather?\(query)&lang=es&units=metric&APPID=18a3df6fc0a08acae2846519cfc6754f"
        print(urlPath)
        let url = URL(string: urlPath)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) {
            
            (data, response, error) in
            
            if(error != nil){
                print(error.debugDescription)
            }else{
                let nsdata:NSData = NSData(data: data!)
                self.parseJson(data: nsdata)
                
                //notify main thread to update vars with data
                DispatchQueue.main.async {
                    print(self.weather)
                    self.lblDescription.text = self.weather
                    self.lblTemp.text = "La temperatura es de \( String(self.temp))º"
                    self.lblCityName.text = self.cityName
                }
            }
        }
        
        task.resume()
    }
    
    /*
     * parse the weather info to json type
     */
    func parseJson(data: NSData){
        do {
            let JSON = try? JSONSerialization.jsonObject(with: data as Data, options: [])
            if let dictionary = JSON as? [String: Any],
               let weatherDic = dictionary["weather"] as? [[String: Any]]
               
            {
                weatherDic.forEach { item in
                    //print(item["description"] as? String)
                    self.weather = item["description"] as? String
                    //print(item["Id"] as? Int)
                    //print(item["Number"] as? Int)
                }
                let mainDic = dictionary["main"] as? [String: Any]
                for (key, value) in mainDic! {
                    if key == "temp" {
                        print("value",value)
                        self.temp = value as? Int
                    }
                }
                
                self.cityName = dictionary["name"] as? String
                print(self.cityName)
            }
         } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        //get weather from api using geolocalization
        getWeatherFromApi(query: "lat=\(userLocation.coordinate.latitude)&lon=\(userLocation.coordinate.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}

